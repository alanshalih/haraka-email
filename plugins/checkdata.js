

exports.register = function () {
	const plugin = this;
	 
	plugin.register_hook('data_post',      'checkdata');

}

exports.bounce_list = {};

exports.checkdata = function (next, connection, params) {
	connection.transaction.header.remove("received")
	connection.transaction.header.remove("authentication-results");
	
	connection.transaction.notes['messageId'] = connection.transaction.header.get('key-id');
	connection.transaction.notes['send_from'] = connection.transaction.header.get('send_from');
	
	connection.transaction.header.remove("send_from");

	return next();
}
